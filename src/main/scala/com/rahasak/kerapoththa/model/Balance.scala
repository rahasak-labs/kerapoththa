package com.rahasak.kerapoththa.model

import com.rahasak.kerapoththa.config.StorageConf
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{ProvenShape, Tag}

case class Balance(id: Long, account: String, value: Long)

trait BalancesTable {
  this: StorageConf =>

  class Balances(tag: Tag) extends Table[Balance](tag, "balances") {
    def id = column[Long]("id", O.PrimaryKey)

    def account = column[String]("account")

    def value = column[Long]("value")

    // select
    def * : ProvenShape[Balance] = (id, account, value) <> (Balance.tupled, Balance.unapply)
  }

  val balances = TableQuery[Balances]
}
