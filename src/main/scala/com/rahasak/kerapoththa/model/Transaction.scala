package com.rahasak.kerapoththa.model

import com.rahasak.kerapoththa.config.StorageConf
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

case class Transaction(id: String, execer: String, messageType: String, message: String, digsig: String, timestamp: Long)

trait TransactionsTable {
  this: StorageConf =>

  class Transactions(tag: Tag) extends Table[Transaction](tag, "transactions") {
    def id = column[String]("id", O.PrimaryKey, O.Unique)

    def execer = column[String]("execer")

    def messageType = column[String]("message_type")

    def message = column[String]("message")

    def digsig = column[String]("digsig")

    def timestamp = column[Long]("timestamp")

    // select
    def * = (id, execer, messageType, message, digsig, timestamp) <> (Transaction.tupled, Transaction.unapply)
  }

  val transactions = TableQuery[Transactions]
}

