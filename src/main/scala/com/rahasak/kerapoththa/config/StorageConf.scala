package com.rahasak.kerapoththa.config

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.PostgresProfile.api._

import scala.util.Try

trait StorageConf {
  val config = ConfigFactory.load("storage.conf")

  // db config
  lazy val dbName = Try(config.getString("postgres.dbName")).getOrElse("kerapoththa")
  lazy val dbHost = Try(config.getString("postgres.host")).getOrElse("192.168.64.31")
  lazy val dbPort = Try(config.getInt("postgres.port")).getOrElse(26257)
  lazy val dbUser = Try(config.getString("postgres.user")).getOrElse("root")
  lazy val dbPass = Try(config.getString("postgres.password")).getOrElse("root")
  lazy val url = s"jdbc:postgresql://$dbHost:$dbPort/$dbName"

  // slick db with hikari connection pool
  val db = {
    val config = new HikariConfig
    config.setDriverClassName("org.postgresql.Driver")
    config.setUsername(dbUser)
    config.setPassword(dbPass)
    config.setJdbcUrl(url)
    config.setConnectionTestQuery("SELECT 1")
    config.setIdleTimeout(300)

    val ds = new HikariDataSource(config)
    Database.forDataSource(ds, Option(20))
  }
}
