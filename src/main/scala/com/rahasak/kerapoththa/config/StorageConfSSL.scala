package com.rahasak.kerapoththa.config

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.PostgresProfile.api._

import scala.util.Try

trait StorageConfSSL {
  val config = ConfigFactory.load("storage.conf")

  // db config
  lazy val dbName = Try(config.getString("postgres.dbName")).getOrElse("kerapoththa")
  lazy val dbHost = Try(config.getString("postgres.host")).getOrElse("192.168.64.31")
  lazy val dbPort = Try(config.getInt("postgres.port")).getOrElse(26257)
  lazy val dbUser = Try(config.getString("postgres.user")).getOrElse("root")
  lazy val dbPass = Try(config.getString("postgres.password")).getOrElse("root")

  // certificates are on /certs dir inside resources
  // load keys from certs dir
  lazy val certsDir = getClass.getClassLoader.getResource("certs").getPath
  lazy val clientCrt = s"$certsDir/client.root.crt"
  lazy val clientKey = s"$certsDir/client.root.key.pk8"
  lazy val caCrt = s"$certsDir/ca.crt"
  lazy val url = s"jdbc:postgresql://$dbHost:$dbPort,$dbHost:26258,$dbHost:26259/$dbName" +
    s"?user=root" +
    s"&ssl=true" +
    s"&sslmode=verify-full" +
    s"&sslcert=$clientCrt" +
    s"&sslkey=$clientKey" +
    s"&sslrootcert=$caCrt"

  // slick db with hikari connection pool
  val db = {
    val config = new HikariConfig
    config.setDriverClassName("org.postgresql.Driver")
    config.setJdbcUrl(url)
    config.setConnectionTestQuery("SELECT 1")
    config.setIdleTimeout(300)

    val ds = new HikariDataSource(config)
    Database.forDataSource(ds, Option(20))
  }
}
