package com.rahasak.kerapoththa

import com.rahasak.kerapoththa.model._
import com.rahasak.kerapoththa.repo.KerapoththaRepo

import scala.concurrent.Await
import scala.concurrent.duration._

object Main extends App {
  // create tables
  Await.result(KerapoththaRepo.init(), 10.seconds)

  // create transaction
  val t = Transaction("021", "eranga", "create", """{"create": "mesg"}""", "digsig", System.currentTimeMillis())
  Await.result(KerapoththaRepo.createTransaction(t), 10.seconds)

  // get transaction
  println(Await.result(KerapoththaRepo.getTransaction("021", "eranga"), 10.seconds))
  // output
  // Some(Transaction(021,eranga,create,{"create": "mesg"},digsig,1650690728999))



  // create block
  val b = Block("1212", List("t1", "t2"), "merkle", "preHash", "hash", "digsig", System.currentTimeMillis())
  Await.result(KerapoththaRepo.createBlock(b), 10.seconds)

  // get block
  println(Await.result(KerapoththaRepo.getBlock("1212"), 10.seconds))
  // output
  // Some(Block(1212,List(t1, t2),merkle,preHash,hash,digsig,1650690729394))



  // create accounts
  Await.result(KerapoththaRepo.createAccount(Account("001", "rahasak", "rahasak pubkey")), 10.seconds)
  Await.result(KerapoththaRepo.createAccount(Account("002", "bassa", "bassa pubkey")), 10.seconds)

  // get account
  println(Await.result(KerapoththaRepo.getAccount("001"), 10.seconds))
  // output
  // Some(Account(001,rahasak,rahasak pubkey))



  // create balances
  Await.result(KerapoththaRepo.createBalance(Balance(1, "001", 1000)), 10.seconds)
  Await.result(KerapoththaRepo.createBalance(Balance(2, "002", 2000)), 10.seconds)

  // transfer
  Await.result(KerapoththaRepo.transfer("001", "002", 100), 10.seconds)

  // get balances
  println(Await.result(KerapoththaRepo.getBalance("001"), 10.seconds))
  println(Await.result(KerapoththaRepo.getBalance("002"), 10.seconds))
  // output
  // Some(Balance(1,001,900))
  // Some(Balance(2,002,2100))

}
